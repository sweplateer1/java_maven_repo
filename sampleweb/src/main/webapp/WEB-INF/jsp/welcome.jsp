<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ include file="/WEB-INF/jsp/template/tags.jsp"%>
<html>

<head>
	<title>Hdiv-Spring-Mvc-Showcase</title>
	<%@ include file="/WEB-INF/jsp/template/head.jsp"%>
</head>

<body>
	<h1>Hdiv Spring Mvc Showcase 2021-07-09</h1>

	<p>데모용 JAVA Maven 샘플 프로젝트 입니다. 2021-07-05 17:54</p>
	<p>These modules follow the "learn by example" school. Be sure to "look under the hood" to see how it's done.</p>

	<!-- For the sake of example, use the page form with this set of links: -->

	<h2>
		<c:url value="/attacks/attacks.html" var="url" />
		<a href="${url}">취약한 예 섹션</a>
	</h2>
	<p>This section is a demonstration of common web application flaws.
		These exercises show how easily can penetration techniques be applied.</p>

	<h2>
		<c:url value="/secure/attacks.html" var="url" />
		<a href="${url}">Hdiv가 해결 한 취약한 예제 섹션</a>
	</h2>
	<p>This section is a demonstration of common web application flaws
		but they are resolved by Hdiv.</p>

	<h2>
		<c:url value="/authenticated/info.html" var="url" />
		<a href="${url}">인증 섹션</a>
	</h2>
	<p>Application authenticated zone. Secured with Spring Security.</p>

	<h2>
		<c:url value="/fileupload/fileupload.html" var="url" />
		<a href="${url}">파일 업로드 예제 섹션</a>
	</h2>
	<p>File Upload example form.</p>

	<%@ include file="/WEB-INF/jsp/template/footer.jsp"%>
</body>
</html>
